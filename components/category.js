import React from 'react'
import styled from 'styled-components'
import {connect} from 'react-redux'

import reducers from '../store/reducers'

const CategoriesBlocks = styled.div`
    display: flex;
    width: 1200px;
    flex-wrap: wrap;
`

const CategoriesItem = styled.div`
    border: 1px solid #424242;
    padding: 5px 20px;
    border-radius: 20px;
    margin: 5px;
    transition: 0.25s;
    cursor: pointer;
    ${props => props.selected ? `
        background: #424242;
        color: #fff
    `:`
        background: transparent;
        color: #424242
    `}
    &:hover{
        box-shadow: 0 0 10px rgba(0,0,0,0.5);
    }
`

const mapStateToProps = (state) => ({
    categories: state.categories
});

class Index extends React.Component {
    applyFilter = filterId => {
        this.props.dispatch({
            type:'ADD_ACTIVE_CATEGORY',
            payload: filterId
        })
        this.forceUpdate()
        this.props.dispatch({
            type:'APPLY_FILTER',
            payload: null
        })
    }
    render() {
        return (
            <CategoriesBlocks>
                {this.props.categories ? this.props.categories.map((item, key) => (
                        <CategoriesItem selected={item.selected} key={key} onClick={ () => this.applyFilter(key) }>
                            {item.title}
                        </CategoriesItem>
                    )): null}
            </CategoriesBlocks>
        )
    }
}

export default connect(mapStateToProps)(Index)