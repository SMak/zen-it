import React from 'react'
import styled from 'styled-components'
import {connect} from 'react-redux'

const Basket = styled.div`
    display: flex;
    width: 390px;
    height: 600px;
    background: #fff;
    padding: 20px;
    flex-direction: column;
`
const BasketTitle = styled.div`
    font-size: 26px;
`
const BasketItem = styled.div`
    width: 100%;
    display: flex;
    align-items: center;
    height: 50px;
    border-bottom: 1px solid #eee;
    justify-content: space-between;
`

const BasketItemQuantity = styled.div``
const BasketItemPrice = styled.div``

const BasketItemTitle = styled.div``

const mapStateToProps = (state) => ({
    basket: state.basket
});

class Index extends React.Component {
    render() {
        return (
            <Basket>
                <BasketTitle>Корзина</BasketTitle>
                {this.props.basket ? this.props.basket.map((item, key) => (
                        <BasketItem key={key}>
                            <BasketItemTitle>{item.title}</BasketItemTitle>
                            <BasketItemQuantity>Колличество: {item.quantity}</BasketItemQuantity>
                            <BasketItemPrice>Цена: {item.price}</BasketItemPrice>
                        </BasketItem>
                    )): null}
            </Basket>
        )
    }
}

export default connect(mapStateToProps)(Index)