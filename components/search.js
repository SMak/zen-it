import React from 'react'
import styled from 'styled-components'
import {connect} from 'react-redux'


const SearchBox = styled.div`
    width: 1200px;
`
const Search = styled.input`
    display: flex;
    width: 300px;
    margin: 20px 0;
    flex-wrap: wrap;
    padding: 7px;
`

const mapStateToProps = (state) => {
    return {
        state: state
    }
}

class Index extends React.Component {

    state = {
        search: null
    }
    
    componentDidUpdate(){
        
    }

    inputHandle = event => {
        let target = event.target;
        let value = target.value;
        let name = target.name;

        this.setState({
            [name]: value
        },this.storeUpdate);

    }

    storeUpdate = () => {
        this.props.dispatch({
            type:'SEARCH',
            payload: this.state.search
        })
    }
    render() {
        return (
            <SearchBox>
                <Search type="text" name='search' value={this.state.search} placeholder="Поиск по списку товаров" onChange={e => this.inputHandle(e)}/>
            </SearchBox>
        )
    }
}

export default connect(mapStateToProps)(Index)