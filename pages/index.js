import React from 'react'
import styled from 'styled-components'
import {connect} from 'react-redux'
import Link from 'next/link';

const List = styled.div`
    display: flex;
    width: 750px;
    min-height: 600px;
    background: #fff;
    flex-direction: column;
`

const ListTitle = styled.div`
    font-size: 26px;
    margin: 20px;
`

const ListItem = styled.div`
    margin: 0 20px;
    padding: 20px 0;
    display: flex;
    justify-content: space-between;
    border-bottom: 1px solid #eee;
    cursor: pointer;
`

const ListQuantity = styled.div`
    width: 20%;
`

const ListPrice = styled.div`
    width: 20%;
    text-align: center;
`

const ListItemTitle = styled.div`
    width: 20%;
`

const mapStateToProps = (state) => {
    return {
        items: state.filteredItems
    }
}

class Index extends React.Component {
    render() {
        return (
            <List>
                <ListTitle>Список товаров</ListTitle>
                {this.props.items ? this.props.items.map((item, key) => (
                    <Link key={key} href="/[items]" as={`/${item.id}`}>
                        <a>
                            <ListItem >
                                <ListItemTitle>{item.title}</ListItemTitle>
                                <ListQuantity>Колличество: {item.quantity}</ListQuantity>
                                <ListPrice>Цена: {item.price}</ListPrice>
                            </ListItem>
                        </a>
                    </Link>
                )):null}
            </List>
        )
    }
}

export default connect(mapStateToProps)(Index)