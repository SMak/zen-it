import React from 'react'
import styled from 'styled-components'
import {connect} from 'react-redux'
import Link from 'next/link';

const Item = styled.div`
    display: flex;
    padding: 20px;
    width: 750px;
    background: #fff;
    flex-direction: column;
`

const ItemTitle = styled.div`
    font-size: 26px;
`

const ItemDesc = styled.div`
    margin-top: 20px;
`

const ItemQuantity = styled.div `
    margin-top: 5px;
    font-size: 14px;
    color: #9E9E9E;
`

const ItemBuy = styled.div`
    display: flex;
    justify-content: flex-end;
    flex-wrap: wrap;
    margin-top: 50px;
`

const ItemBuyQuantity = styled.div`
    display: flex;
    align-items: center;
`

const ItemBuyQuantityAdd = styled.img`
    width:20px;
    height: 20px;
    margin-right: 10px;
    cursor: pointer;
`

const ItemBuyQuantityRemove = styled.img`
    width:20px;
    height: 20px;
    margin-right: 10px;
    cursor: pointer;
`

const ItemAddBasket = styled.div`
    padding: 5px 20px;
    background: #424242;
    color: #fff;
    cursor: pointer;
`

const ItemBuyQuantityData = styled.div`
    margin-right: 10px;
`

const mapStateToProps = (state) => {
    return {
    single: state.single
}};

class Index extends React.Component {
    state = {
        buyQuantity:1
    }
    componentDidMount(){
        this.props.dispatch({
            type:'GET_SINGLE',
            payload: window.location.pathname.replace('/','')
        })
    }

    addQuantity = () => {
        if(this.state.buyQuantity < this.props.single.quantity){
            this.setState({
                buyQuantity:this.state.buyQuantity +1
            })
        }
    }
    removeQuantity = () => {
        if(this.state.buyQuantity > 1){
            this.setState({
                buyQuantity:this.state.buyQuantity -1
            })
        }
    }

    addBasket = () => {
        this.props.dispatch({
            type:'ADD_BASKET',
            payload: {
                item: this.props.single.id,
                quantity: this.state.buyQuantity
            }
        })
    }

    render() {
        return (
            <Item>
                {this.props.single ?
                    <React.Fragment>
                        <Link href ="/"><a>Назад</a></Link>
                        <ItemTitle>{this.props.single.title}</ItemTitle>
                        <ItemQuantity>{`Колличество - ${this.props.single.quantity} штук`}</ItemQuantity>
                        <ItemDesc>
                            К сожалению в предосталеном файле не было заполнено описание товара, пришлось сгенерировать немного рыбы <br/><br/>
                            Значимость этих проблем настолько очевидна, что постоянное информационно-пропагандистское обеспечение нашей деятельности представляет собой интересный эксперимент проверки дальнейших направлений развития. Идейные соображения высшего порядка, а также консультация с широким активом в значительной степени обуславливает создание соответствующий условий активизации. Повседневная практика показывает, что дальнейшее развитие различных форм деятельности влечет за собой процесс внедрения и модернизации новых предложений.
                            <br/><br/>
                            С другой стороны рамки и место обучения кадров позволяет оценить значение существенных финансовых и административных условий. Значимость этих проблем настолько очевидна, что укрепление и развитие структуры позволяет оценить значение направлений прогрессивного развития. Задача организации, в особенности же дальнейшее развитие различных форм деятельности способствует подготовки и реализации системы обучения кадров, соответствует насущным потребностям. Не следует, однако забывать, что постоянное информационно-пропагандистское обеспечение нашей деятельности позволяет выполнять важные задания по разработке форм развития.
                        </ItemDesc>
                        <ItemBuy>
                            <ItemBuyQuantity>
                                <ItemBuyQuantityAdd onClick={this.addQuantity} src="/static/add.svg"/>
                                <ItemBuyQuantityRemove onClick={this.removeQuantity} src="/static/remove.svg"/>
                                <ItemBuyQuantityData>X {this.state.buyQuantity}</ItemBuyQuantityData>
                                <ItemAddBasket onClick={this.addBasket}>Добавить в корзину</ItemAddBasket>
                            </ItemBuyQuantity>
                        </ItemBuy>
                    </React.Fragment>
                    : null
                }
                
            </Item>
        )
    }
}

export default connect(mapStateToProps)(Index)

//window.location.pathname.replace('/','')