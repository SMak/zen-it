import React from 'react';
import App, { Container } from 'next/app';
import { createGlobalStyle } from 'styled-components';
import axios from 'axios'
import { Provider } from 'react-redux';
import {createStore} from 'redux'
import styled from 'styled-components'

import Categories from '../components/category'
import reducers from '../store/reducers'
import Basket from '../components/basket'
import Search from '../components/search'

const GlobalStyles = createGlobalStyle`
    @import url('https://fonts.googleapis.com/css?family=Roboto:100,300,400,500');
    body {
        margin: 0;
        font-family: 'Roboto', sans-serif;
        font-weight: 400;
        background: #EFEFF4;
    }
    * {
        box-sizing: border-box;
    }
    a {
        text-decoration: none;
        color: unset; 
    }
`;

const Page = styled.div`
    width: 100%;
    min-height: 100vh;
    display: flex;
    padding-top: 50px;
    align-items: center; 
    flex-direction: column;
`

const PageContent = styled.div`
    display: flex;
    justify-content: space-between;
    width: 1200px;
`

const store = createStore(reducers);

class MyApp extends App {

    static async getInitialProps() {
        const getCategories = await axios(`https://zen-it-git-redux.s-mak.now.sh/api/categories`).then((response) => {
            return response.data
        });
        const getItems = await axios('https://zen-it-git-redux.s-mak.now.sh/api/items').then((response) => {
            return response.data
        });
        return {
            getCategories:getCategories,
            getItems:getItems
        };
    }
    
    constructor(props){
        super(props)
        store.dispatch({
            type:'INIT_CATEGORIES',
            payload: this.props.getCategories
        })
        store.dispatch({
            type:'INIT_ITEMS',
            payload: this.props.getItems
        })
    }

    categoryFilter = (filtered) => {
        this.setState({
            context:{
                categories:{
                    data: this.props.categoriesData
                },
                items: {
                    data: this.state.context.items.data.filter(item => item.category_id === filtered),
                    categoryFilter: (data) => this.categoryFilter(data)
                }
            }
        })
    }

    render() {
        const { Component, pageProps } = this.props;
        return (
            <Provider store={store}>
                <Page>
                    <GlobalStyles/>
                        <Categories />
                        <Search />
                        <PageContent>
                            <Container>
                                <Component {...pageProps} />
                            </Container>
                            <Basket/>
                        </PageContent>
                </Page>
            </Provider>
        );
    }
}

export default MyApp;