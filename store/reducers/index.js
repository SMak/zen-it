const reducer = (state = {}, action) => {
    switch (action.type) {
        case 'INIT_CATEGORIES':
            return {...state, initCategories: action.payload, categories: action.payload}
        case 'INIT_ITEMS':
            return {...state, initItems: action.payload, filteredItems:action.payload}
        case 'ADD_ACTIVE_CATEGORY':
            state.categories[action.payload].selected = !state.categories[action.payload].selected
            return {...state};
        case 'APPLY_FILTER':
            let activeCategories = []
            state.categories.map((item) => {
                item.selected ? activeCategories.push(item.id) : null
            })
            if(activeCategories.length > 0){
                return {...state, filteredItems: state.initItems.filter(item => activeCategories.includes(item.category_id))}
            } else {
                return {...state, filteredItems:state.initItems}
            }
        case 'SEARCH':
            let searchData = []
            state.filteredItems.map((item) => {
                if(item.title.indexOf(action.payload) != -1){
                    searchData.push(item)
                }
            })
            if(searchData.length > 0){
                return {...state, filteredItems: searchData};
            } else {
                return {...state, filteredItems:state.initItems};
            }
        case 'GET_SINGLE':
            let single
            state.initItems.map((item) => {
                if(item.id.toString() === action.payload){
                    single = item
                }
            })
            return {...state, single:single}
        case 'ADD_BASKET':
            let basket = state.basket ? state.basket : []
            state.initItems.map((item) => {
                if(item.id === action.payload.item){
                    basket.push({...item, quantity:action.payload.quantity})
                }
            })
            return {...state, basket:basket}
        default:
            return state;
    }
};

export default reducer

//filteredItems: state.initItems.filter(item => item.category_id === action.payload+1)